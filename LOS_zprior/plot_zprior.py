#!/usr/bin/env python3

import matplotlib.pyplot as plt
import h5py
import gwcosmo

def plot_zprior(infile, outfile, pixel_index):
    fig = plt.figure()
    ax = fig.add_subplot()

    LOS_catalog = h5py.File(infile, 'r')

    z_array = gwcosmo.utilities.zprior_utilities.get_z_array(LOS_catalog)
    zprior = gwcosmo.utilities.zprior_utilities.get_zprior(LOS_catalog, pixel_index)
    zprior_empty = gwcosmo.utilities.zprior_utilities.get_empty_catalog(LOS_catalog)

    ax.plot(z_array, zprior, label="with catalogue")
    ax.plot(z_array, zprior_empty, label="empty catalogue")

    ax.set_xlim(0, 0.2)
    ax.set_ylim(0,0.02)
    ax.set_xlabel(r"$z$")
    ax.set_ylabel(r"$p(z)$")
    ax.set_title(f"zprior pixel {pixel_index}")
    ax.legend()

    plt.savefig(outfile)

def main():
    pixel_index = 130
    filename = f"./GLADE+_LOS_redshift_prior_K_band_luminosity_weighted_nside_32_pixel_index_{pixel_index}"
    infile = f"{filename}.hdf5"
    outfile = f"{filename}.png"

    plot_zprior(infile, outfile, pixel_index)

if __name__ == "__main__":
    main()
    
    
