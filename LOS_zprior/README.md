# The generation of the LOS redshift prior

## Preparations
Before generating a LOS redshift prior, the path to the folder containing the galaxy catalogue should be set, as this is not a part of `gwcosmo`.
```
export GWCOSMO_CATALOG_PATH=<path_to_catalogue_folder>
```
The code makes use of skymaps containing precomputed constants a low angular resolution. These can be generated [here](https://git.ligo.org/rachel.gray/gwcosmo-workshop-2/-/tree/main/maps).
or they can be calculated automatically before generating the LOS redshift prior itself.

## Generating the LOS redshift prior for one pixel

Below is a command line example for generating the LOS redshift prior for `pixel_index 130`. Be aware that the script looks for specific map titles in the `maps_path` folder, as they should contain the same constants that are used when calculating the redshift prior. If no matching file is found, new files corresponding to `pixel_index` will be created in the same folder. This command will produce a file called **GLADE+_LOS_redshift_prior_K_band_luminosity_weighted_nside_32_pixel_index_130.hdf5**.

```
../../gwcosmo/bin/gwcosmo_compute_redshift_prior \
--catalog GLADE+ \
--nside 32 \
--catalog_band K \
--Kcorr True  \
--min_gals_for_threshold 10 \
--maps_path ../data \
--pixel_index 130
```

Optional options can be shown using ```--help```.
```
../../gwcosmo/bin/gwcosmo_compute_redshift_prior --help
```

The resulting LOS redshift prior file can be plotted using [this script](https://git.ligo.org/rachel.gray/gwcosmo-workshop-2/-/blob/main/LOS_zprior/plot_zprior.py) as shown below. This will create a file called **GLADE+_LOS_redshift_prior_K_band_luminosity_weighted_nside_32_pixel_index_130.png**.
```
python ./plot_zprior.py
```

## Generating the LOS redshift prior for the full sky

The command line below will generate a LOS zprior file for the full sky, for which the ```pixel_index``` argument should not be specified. The code is can be parallelized on the level of the pixels, for which ```num_threads``` should be specified. For ```nside 32``` the command should finish in a few hours. The resulting file is called
**GLADE+_LOS_redshift_prior_K_band_luminosity_weighted_nside_32_pixel_index_None.hdf5**.

```
../../gwcosmo/bin/gwcosmo_compute_redshift_prior \
--catalog GLADE+ \
--nside 32 \
--catalog_band K \
--Kcorr True \
--min_gals_for_threshold 10 \
--maps_path ../data \
--num_threads 8
```
