# Getting started

In order to run the material in this workshop you must have [gwcosmo](https://git.ligo.org/cbc-cosmo/gwcosmo/-/tree/mcmc) installed. If you don't already have it installed, follow the instructions in gwcosmo's README.


# Running examples

In order to run each of the following examples, make sure you have your gwcosmo environment activated. The command lines provided here assume that gwcosmo and gwcosmo-workshop-2 are in the same directory, but you can easily update the file paths if your setup differs.

To run each set of examples you should cd into the relevant directory. All paths are set up so they are correct relative to the required data when run in that directory.

Note: this git repository contains a set of 200,000 injections to be used with this tutorial. A larger set of 2 million injections can be found at on the CIT cluster at **/home/cbc.cosmology/injections/O1_O2_O3/inj_SNR9_det_frame_2e6.h5**, which will give a smoother result.

## EM counterpart

This analysis does not require any additional data to be downloaded, and can be run as a quick check that your gwcosmo install is working.

## Dark siren and galaxy catalogue

In order to run the dark siren & galaxy catalogue examples you will need to download a pre-computed line-of-sight (LOS) redshift prior and put it in the data folder. Some pre-computed LOS priors can be [downloaded here](https://ldas-jobs.ligo.caltech.edu/~freija.beirnaert/gwcosmo_tutorial/LOS_zpriors/). The examples are set up to use **GLADE+_LOS_redshift_prior_K_band_luminosity_weighted_nside_32_pixel_index_None.hdf5**, which is the smallest file. It has a resolution of nside=32. To obtain a more precise result, use a higher resolution LOS prior, such as nside 64 or 128. In this tutorial, because the localisation volumes of the events in question are quite large, this will have minimal impact on the results.

## Line-of-sight prior
In order to run the line-of-sight prior examples you will need to download GLADE 2.4 or GLADE+ in a gwcosmo-compatible format and place it in the data folder. For ease, if you do not have either of these catalogue pre-downloaded, they can be [downloaded here](https://ldas-jobs.ligo.caltech.edu/~freija.beirnaert/gwcosmo_tutorial/catalogues/).


