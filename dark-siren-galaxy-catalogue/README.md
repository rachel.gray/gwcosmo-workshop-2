# The dark siren & galaxy catalogue analysis

Here we introduce the dark siren and galaxy catalogue method. A simple 1-dimensional example for a single GW event is shown first. The final analyses we expect to run in O4 will be multiparameter analyses using a large number of gravitational wave events. Note that here we use injections which generated with O1+O2+O3 sensitivity, and apply an SNR threshold of 11, meaning that to be self-consistent the final posterior produced should include all O1+O2+O3 BBHs with SNR>11. For simplicity, here we use a smaller subset of events, so these results should not be treated as scientifically rigorous. (Note, if running with the gridded method, the analysis can easily be run for individual events and then combined at a later stage, but for the sampling method the analysis should be run jointly on all events from the beginning).

## Single-parameter estimation using a single GW event

Try running the following. It will produce two files: **GW150914.npz** and **GW150914.png**. 

```
../../gwcosmo/bin/gwcosmo_dark_siren_posterior \
--method gridded \
--posterior_samples ../data/GW150914_GWTC-1.hdf5 \
--skymap ../data/GW150914_skymap.fits.gz \
--injections_path ../data/inj_SNR9_det_frame_2e5.h5 \
--LOS_catalog ../data/GLADE+_LOS_redshift_prior_K_band_luminosity_weighted_nside_32_pixel_index_None.hdf5 \
--parameter_dict ../data/parameter_dictionary.json \
--redshift_evolution Madau \
--mass_model BBH-powerlaw-gaussian \
--snr 11. \
--outputfile GW150914
```

Try changing the mass model, or the mass model hyperparameters, or the redshift evolution parameters, to see how this impacts the result.


## Multi-event parameter estimation

On many occasions you will want to run the analysis for multiple GW events. In this case, simply use the `--posterior_samples` and `--skymap` arguments to point to a dictionary which includes the paths for each event. Note that the time for a single likelihood evaluation increases linearly with the number of events.

```
../../gwcosmo/bin/gwcosmo_dark_siren_posterior \
--method gridded \
--posterior_samples ../data/posterior_samples_dictionary.json \
--skymap ../data/skymaps_dictionary.json \
--injections_path ../data/inj_SNR9_det_frame_2e5.h5 \
--LOS_catalog ../data/GLADE+_LOS_redshift_prior_K_band_luminosity_weighted_nside_32_pixel_index_None.hdf5 \
--parameter_dict ../data/parameter_dictionary.json \
--redshift_evolution Madau \
--mass_model BBH-powerlaw-gaussian \
--snr 11. \
--outputfile O1_O2_BBHs
```

In general it is good practice to put the absolute paths in the samples and skymap dictionaries, so that if you change directories you don't have to update the paths.


## Multiparameter estimation using nested sampling

It is strongly recommended to using sampling when running more than two parameters. For the BBH-powerlaw-gaussian mass model, some parameters of interest to jointly estimate are **gamma** (a merger rate evolution parameter), **mmaxbh** (the maximum black hole mass) and **mu_g** (the location of the gaussian peak in the mass distribution), as all of these parameters correlate strongly with H0.

The following can be run from the command line, but it is recommended that for sampling you submit a job to a cluster and make use of multiple cores in order to speed up your analysis.

```
../../gwcosmo/bin/gwcosmo_dark_siren_posterior \
--method sampling \
--posterior_samples ../data/posterior_samples_dictionary.json \
--skymap ../data/skymaps_dictionary.json \
--injections_path ../data/inj_SNR9_det_frame_2e5.h5 \
--LOS_catalog ../data/GLADE+_LOS_redshift_prior_K_band_luminosity_weighted_nside_32_pixel_index_None.hdf5 \
--parameter_dict ../data/parameter_dictionary.json \
--redshift_evolution Madau \
--mass_model BBH-powerlaw-gaussian \
--snr 11. \
--sampler dynesty \
--nlive 1000 \
--dlogz 0.01 \
--outputfile O1_O2_BBHs
```

**Suggested run settings:** It is up to you how many cores you request, as it will depend on the cluster you are using and how much space is available. If, for example, you request 20 cores you will also need to include the argument `--npool 20`.
