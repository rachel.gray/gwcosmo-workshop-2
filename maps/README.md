# The generation of coarse skymaps
## Preparations
Before generating a map, the path to the folder containing the galaxy catalogue should be set, as this is not a part of ```gwcosmo```.
```
export GWCOSMO_CATALOG_PATH=<path_to_catalogue_folder>
```

## Generating coarse skymaps
[This script](https://git.ligo.org/rachel.gray/gwcosmo-workshop-2/-/blob/main/maps/create_maps.py) demonstrates how to generate and plot skymaps at a low resolution. It can be run with the command below. It takes only several minutes to run, as the resolution is set very low. The resulting files are called **mth_map_GLADE+_bandK_nside8_pixel_indexNone_mingals10.fits** and **norm_map_GLADE+_bandK_nside8_pixel_indexNone_zmax10,0_zcut0,5_Mmax-19,0_KcorrTrue.fits**, and the figures **mth_map.png** and **norm_map.png**.
```
python ./create_maps.py
```

For the generation of the LOS redshift prior, one should use the maps [here](https://git.ligo.org/rachel.gray/gwcosmo-workshop-2/-/tree/main/data).