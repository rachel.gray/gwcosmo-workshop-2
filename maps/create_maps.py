#!/usr/bin/env python3

import matplotlib.pyplot as plt
import healpy as hp
import gwcosmo

def plot_map(infile, outfile, title, unit):
    m = hp.fitsfunc.read_map(infile, nest=True)

    hp.mollview(m, title=title, unit=unit, nest=True)
    plt.savefig(outfile)

def main():
    catalog = "GLADE+"
    coarse_nside = 8
    band = "K"
    min_gals_for_threshold = 10
    zmax = 10.
    zcut = 0.5
    schech_Mmax = -19.
    apply_Kcorr = True
    coarse_pixel_index = None
    H0 = 70.
    Omega_m = 0.3065
    w0 = -1.
    wa = 0.
    cosmology = gwcosmo.utilities.cosmology.standard_cosmology(H0, Omega_m, w0, wa)

    # create and plot mth file
    print("generate mth map")
    mth_file = f"./mth_map_{catalog}_band{band}_nside{coarse_nside}_pixel_indexNone_mingals{min_gals_for_threshold}.fits"
    gwcosmo.maps.create_mth_map.create_mth_map(mth_file, catalog, band, coarse_nside, min_gals_for_threshold, coarse_pixel_index)
    plot_map(mth_file, "./mth_map.png", "mth map", r"$m_{th}$")
    
    # create and plot norm file
    print("generate norm map")
    norm_file = f"./norm_map_{catalog}_band{band}_nside{coarse_nside}_pixel_index{coarse_pixel_index}_zmax{str(zmax).replace('.', ',')}_zcut{str(zcut).replace('.', ',')}_Mmax{str(schech_Mmax).replace('.', ',')}_Kcorr{apply_Kcorr}.fits"
    gwcosmo.maps.create_norm_map.create_norm_map(norm_file, catalog, band, coarse_nside, coarse_pixel_index, zmax, zcut, mth_file, cosmology, schech_Mmax, apply_Kcorr)
    plot_map(norm_file, "./norm_map.png", "norm map", r"galaxy_norm")

if __name__ == "__main__":
    main()
    
    