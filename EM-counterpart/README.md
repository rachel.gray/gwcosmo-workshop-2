# The EM counterpart analysis

Below are two command line examples of running the EM counterpart analysis. Both will perform an estimation of H0 for GW170817 with its EM counterpart, using a gridded method, and will produce two files: **GW170817.npz** and **GW170817.png**. 

## Using posterior samples conditioned on the counterpart line-of-sight

This analysis makes use of the posterior samples for GW170817 which were conditioned on the line-of-sight of NGC 4993.

```
../../gwcosmo/bin/gwcosmo_bright_siren_posterior \
--method gridded \
--posterior_samples ../data/GW170817_GWTC-1.hdf5 \
--injections_path ../data/inj_SNR9_det_frame_2e5.h5 \
--parameter_dict ../data/parameter_dictionary.json \
--redshift_evolution Madau \
--mass_model BNS \
--snr 11. \
--counterpart_v 3017 \
--counterpart_sigmav 166 \
--outputfile GW170817
```

If using a set of posterior samples which are *not* conditioned on the line-of-sight of the counterpart, you can simply include the argument `--post_los False` and also provide values for `--counterpart_ra` and `--counterpart_dec` which must be provided in radians.

## Using the skymap

This analysis makes use of the skymap for GW170817.

```
../../gwcosmo/bin/gwcosmo_bright_siren_posterior \
--method gridded \
--skymap ../data/GW170817_skymap.fits.gz \
--injections_path ../data/inj_SNR9_det_frame_2e5.h5 \
--parameter_dict ../data/parameter_dictionary.json \
--redshift_evolution Madau \
--mass_model BNS \
--snr 11. \
--counterpart_v 3017 \
--counterpart_sigmav 166 \
--counterpart_ra 3.446023854265526  \
--counterpart_dec -0.40813554930525175 \
--outputfile GW170817
```

## Multiparameter estimation with an EM counterpart

Try updating the multiparameter dictionary to use H0=[20, 140, 50], and Xi0=[0.5,5,20] then run the following:

```
../../gwcosmo/bin/gwcosmo_bright_siren_posterior \
--method gridded \
--posterior_samples ../data/GW170817_GWTC-1.hdf5 \
--injections_path ../data/inj_SNR9_det_frame_2e5.h5 \
--parameter_dict ../data/parameter_dictionary.json \
--redshift_evolution Madau \
--mass_model BNS \
--snr 11. \
--counterpart_v 3017 \
--counterpart_sigmav 166 \
--gravity_model Xi0_n \
--outputfile GW170817_MG
```

